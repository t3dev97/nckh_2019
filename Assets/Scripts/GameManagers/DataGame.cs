﻿using System.Collections.Generic;
using UnityEngine;

public class DataGame : MonoBehaviour
{
    [Header("List")]
    public List<GameObject> listItem;
    public List<GameObject> listEnemy;
    public List<GameObject> listPlayer;
    public List<GameObject> listDragon;

    [Header("Container")]
    public Transform CTN_Item;
    public Transform CTN_Enemy;
    public Transform CTN_Player;
    public Transform CTN_Dragon;

    public GameObject GetObject(int index, OBJECT_TYPE oBJECT_TYPE)
    {
        switch (oBJECT_TYPE)
        {
            case OBJECT_TYPE.item:
                if (CheckIndex(index, oBJECT_TYPE))
                    return Instantiate(listItem[index], CTN_Item);
                else
                    return null;

            case OBJECT_TYPE.enemy:
                if (CheckIndex(index, oBJECT_TYPE))
                    return Instantiate(listEnemy[index], CTN_Enemy);
                else
                    return null;

            case OBJECT_TYPE.player:
                if (CheckIndex(index, oBJECT_TYPE))
                    return Instantiate(listPlayer[index], CTN_Player);
                else
                    return null;

            default: return null;
        }
    }
    bool CheckIndex(int index, OBJECT_TYPE oBJECT_TYPE)
    {
        switch (oBJECT_TYPE)
        {
            case OBJECT_TYPE.item:
                if (listItem.Count > 0 && (index >= 0 && index < listItem.Count)) return true;
                return false;
            case OBJECT_TYPE.enemy:
                if (listEnemy.Count > 0 && (index >= 0 && index < listEnemy.Count)) return true;
                return false;
            case OBJECT_TYPE.player:
                if (listPlayer.Count > 0 && (index >= 0 && index < listPlayer.Count)) return true;
                return false;
            default:
                return false;
        }
    }
}
