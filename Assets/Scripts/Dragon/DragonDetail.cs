﻿using UnityEngine;

public class DragonDetail : MonoBehaviour
{
    [SerializeField]
    float _offsetX;

    [SerializeField]
    float _speedMove;

    Rigidbody _rb;
    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }
    public float OffSetX { get => _offsetX; set => _offsetX = value; }
    public Rigidbody Rb { get => _rb; set => _rb = value; }
    public float SpeedMove { get => _speedMove; set => _speedMove = value; }
}
