﻿using UnityEngine;

[RequireComponent(typeof(DragonDetail))]
[RequireComponent(typeof(DragonController))]
public abstract class DragonDoSomeThing : MonoBehaviour
{
    DragonDetail _myDetail;
    DragonController _myController;

    public DragonDetail MyDetail { get => _myDetail; set => _myDetail = value; }
    public DragonController MyController { get => _myController; set => _myController = value; }

    public void ReferenceComponent()
    {
        _myDetail = GetComponent<DragonDetail>();
        _myController = GetComponent<DragonController>();
    }
}
