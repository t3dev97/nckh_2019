﻿using UnityEngine;

public class DragonController : MonoBehaviour
{
    [SerializeField]
    bool _allowMove;

    [SerializeField]
    bool _allowDropItem;

    public bool AllowMove
    {
        get => _allowMove;
        set
        {
            _allowMove = value;
            if (_allowMove == true)
            {
                var move = GetComponent<DragonMove>();
                if (move != null)
                    StartCoroutine(move.Move(_allowMove));
            }
        }
    }
    public bool AllowDropItem { get => _allowDropItem; set => _allowDropItem = value; }

    private void Start()
    {
        AllowMove = true;
    }

}
